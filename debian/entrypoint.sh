#!/bin/sh

echo $USERNAME > /etc/openvpn/auth.txt
echo $PASSWORD >> /etc/openvpn/auth.txt

# nordvpn login -u $USERNAME -p $PASSWORD
# nordvpn c -f

exec openvpn --config /etc/openvpn/ovpn_$PROTOCOL/$SERVER.nordvpn.com.$PROTOCOL.ovpn --auth-user-pass /etc/openvpn/auth.txt --daemon openvpn
# openvpn /etc/openvpn/ovpn_$PROTOCOL/$SERVER.nordvpn.com.$PROTOCOL.ovpn
# while true; echo "/etc/openvpn/ovpn_$PROTOCOL/$SERVER.nordvpn.com.$PROTOCOL.ovpn"; sleep 5000; done
