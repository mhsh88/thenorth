FROM alpine

# RUN echo "@nordvpn https://nordvpn.com/api/packages/nordvpn-stable/v3/alpine/\$alpine" >> /etc/apk/repositories
# RUN apk update --no-cache
# RUN apk add nordvpn

RUN apk add --no-cache openvpn curl
RUN curl -O https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip && \
    unzip ovpn.zip -d /etc/openvpn
# RUN curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh -o install.sh
# RUN chmod +x install.sh
# RUN /bin/sh install.sh
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
CMD ["/entrypoint.sh"]